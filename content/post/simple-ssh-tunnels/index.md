+++
author = "unkwn1"
title = "Creating SSH tunnels easily with ProxyCommand using netcat and, ssh's built-in JumpHost option."
date = "2022-02-13"
description = "Creating a proxy between my main server and my personal devices using SSH."
toc = true
series = [
	"devops",
]
tags = [
    "ssh",
	"networking",
	"netcat",
]
categories = [
    "SysAdmin",
]
+++


[![free-proxies.gif](https://i.postimg.cc/htrt8G4k/free-proxies.gif)](https://postimg.cc/nXXxppD2)

# Creating an SSH Tunnel

I wanted to add a bit of security when connecting to my main server. Currently my security consists of an uncommon port, no root access and, key only access. I tend to rotate my device' IP addresses which as ruled out a UFW rule - with this I'd be content under normal circumstances.


## SSH Built in JumpHost

Assuming you have HostA and HostB set up in your `~/.ssh/config` file creating a tunnel is a simple one-liner.

`ssh -J HostB HostA`

This will connect from your client to HostB and then onwards to HostA.

I'm currently using this method. This allows me to rotate my personal device IPs connecting to an intermediary server with a static IP which is on my main server's allow list.

> *BONUS* add some extra layers using multiple `-J` flags in your chain.
> `ssh -J HostC -J HostB HostA`

### JumpHost + PortForward

Add `-L <PORT>:<LOCALHOST>:<PORT>` to the above proxychain to create a secured means to forward local ports. 

## netcat Based Proxy

With the use of `netcat` we can setup a `ProxyCommand` option on the `Host` in the `~/.ssh/config` file that will create the tunnel from HostB to HostA.

> *NOTE* the `ProxyCommand` option can be passed via the `ssh` cli using a flag `-o "ProxyCommand <nc command>"`

```config
# Assume you have HOSTA configured above 
# without the ProxyCommand option

Host <NAME>
	HostName 		<IP>
	Port 			<PORT>
	IdentityFile 	<PATH>
	Compression 	<YES>
	ProxyCommand 	ssh HOSTA nc %h %p```
