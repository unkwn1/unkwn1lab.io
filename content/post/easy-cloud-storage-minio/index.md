+++
author = "unkwn1"
title = "Super Simple Cloud Storage using Minio"
date = "2022-10-05"
description = "A simple summary of how I deployed an instance of Minio on my VPS with SSL support."
toc = true
tags = [
    "linux",
    "debian",
    "cloud storage",
]
categories = [
    "Projects",
    "DevOps",
    "SysAdmin",
]
+++
![](https://www.vaisulweb.com/wp-content/uploads/2019/03/s3_intro_800.png)

I'm in love with my [Swissmade.host server](https://swissmade.host/?ref=jfogarty), the resource to price is outstanding. For roughly $17 CAD a month I get 4GB ram, 2CPU, 60GB NVME and, 4TB bandwidth... How could you not pick up a swissmade.host vps!? What better way to use my swissmade server then as offsite backup storage (among other uses). Not sure where to start after a couple minutes Googling I came across the project Minio.

Minio is an S3 compatible object storage solution that is as robust as it is convenient. This article will go over my implementation - which I hope to update soon with redundancies in order to make the storage "highly available".

**Dependencies**
  - A Linux sever
  - docker / docker-compose

## Package Manager Installation
Minio is included in `apt` and `pacman` respectively. For Debian users run `sudo apt install minio`.

Once installed you can double check by starting the Minio service `sudo systemctl start minio`. If you don't get the login url, you can run `sudo systemctl status minio` which will include the bound UI port and the API port.

## Setting a Web-UI Port
A minor annoyance is due to minio attempting to serve the UI via a random port each run. To make it static we're going to modify the systemd service file `/etc/systemd/system/minio.service`.

Open the file and look for the `ExecStart` line. Add the argument `--console-address <ip>:<port>` changing the to suit your environment.

`ExecStart=/usr/local/bin/minio server --console-address <ip>:<port> $MINIO_OPTS $MINIO_VOLUMES`

## Sub-domain & SSL - The Lazy Way
I'm a [huge fan](https://unkwn1.dev/post/my-nginx-proxy-manager-review/) of the [nginx proxy manager project](https://github.com/NginxProxyManager/nginx-proxy-manager) and will be using it for my minio setup. So, if you haven't already set up [nginx proxy manager](https://nginxproxymanager.com/) and come back!

First, make sure you've set up an DNS A record for your minio subdomain pointing to your server. Then create a new proxy host in nginx proxy manager pointing to your server's ip/port. Click the SSL take and "request a new certificate". 

> :memo: **NOTE** I ran into a small issue with using the force https option. I've disabled it in nginx proxy manager and set up the redirect in CloudFlare. I presume the proxy is missing some header info."
