+++
author = "unkwn1"
title = "Managing Tmux Sessions with Tmuxp" 
date = "2024-01-14"
description = "One of the things I love about tmuxp is that it lets me set up my layouts exactly how I want them. I can create a tmux session, split the panes as I like"
tags = [
    "tmux",
    "tmuxinator",
    "tmuxp",
]
categories = [
    "sysadmin",
]
series = [
    "CLI Tools",
]
+++

![](images/tmux-logo.png)

If you are a tmux user, you might be familiar with tmuxinator, a tool that helps you manage your tmux sessions. I used to rely on tmuxinator, but I found it cumbersome to install and use on RHEL. That's why I switched to tmuxp, and I'm glad I did. Tmuxp is fantastic.

Tmuxp is a tool that helps you manage tmux workspaces. It is built on an object relational mapper for tmux. You can reload common workspaces from YAML, JSON, and `dict`") workspace files, just like tmuxinator and teamocil.

One of the things I love about tmuxp is that it lets me set up my layouts exactly how I want them. I can create a tmux session, split the panes as I like, and then run `tmux lsw` to get the layout code. Tmuxp can use this code to recreate the layout, instead of relying on presets like `main-vertical`.

Another cool feature of tmuxp is that it supports before and after scripts. This means I can easily set up a python development session, where I `cd` into a project directory, activate the virtual environment, and then open the panes in the same environment.

Tmuxp has many more features that I have yet to explore, such as its plugin system. The project is written in python, which makes it easy to extend and customize. Tmuxp is a powerful tool for boosting your terminal productivity, and I highly recommend it to anyone who uses tmux.

